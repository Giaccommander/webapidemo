﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPIotas.Models
{
    public class RegisterModel: IEnumerator,IEnumerable
    {

        public string Email { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Street { get; set; }

        public string Zip { get; set; }

        public string Country { get; set; }

        public string Land { get; set; }

        public string TelefonNumber { get; set; }

        public string Anrede { get; set; }
        object IEnumerator.Current { get; }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        bool IEnumerator.MoveNext()
        {
            throw new NotImplementedException();
        }

        void IEnumerator.Reset()
        {
            throw new NotImplementedException();
        }
    }
    public class RegisterResponseObject
    {

        public string description { get; set; }
        public string status { get; set; }
        public int code { get; set; }
        public string value { get; set; }
        public string httpResponse { get; set; }


    }
}