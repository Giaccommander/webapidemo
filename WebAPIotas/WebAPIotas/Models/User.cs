﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace WebAPIotas.Models
{
    public class User
    {

        public int Id { get; set; }        
        public string Anrede { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Street { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Land { get; set; }
        public string TelefonNumber { get; set; }
        public string Activationhash { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime? ActivatedAt { get; set; }


    }
}