﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPIotas.Models
{

    public class LoginModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

    }

    public class ResponseObject
    {
        public string description { get; set; }
        public string status { get; set; }
        public int code { get; set; }
        public string value { get; set; }

        private string _Hash;
        public string Hash
        {
            get
            {
                if (_Hash == null)
                {
                    _Hash = "?";
                }
                return _Hash;
            }
            set
            {
                if (value == null)
                {
                    _Hash = "?";
                }
                _Hash = value;
            }
        }



    }
}