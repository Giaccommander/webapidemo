﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPIotas.Models;
using Dapper;
using System.Data;
using WebAPIotas.Helpers;

namespace WebAPIotas
{
    public class DataAccess
    {
        public User GetUserDb( string email)
        {            
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(DapperHelper.CnnVal("LeikermoserDB")))
            {
                return connection.Query<User>($"select * from App.Users where email = '{email}'").SingleOrDefault();
            }            
        }
    }
}