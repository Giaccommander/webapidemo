﻿
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register("../sw.js")
        .then(function () {
            console.log("ServiceWorker in navigator");
        });

}
//--------------------------------------------------- GLOBAL Elements ---------------------------------------------------------------------
let button1 = document.getElementById("loginButton");
let deferredPrompt;
let currentHash = window.location.hash;
const nl = document.querySelectorAll(".page");
const allPages = [];
let storageValue = localStorage.getItem("hash");
let sessionValue = sessionStorage.getItem("hash");
//--------------------------------------------------- /GLOBAL Elements ---------------------------------------------------------------------



//------------------------------------------------- hashChange und PageSelector ------------------------------------------------------------

function sessionCheck() {

    if (sessionValue === null || sessionValue === "") {
        if (storageValue === null || storageValue === "") {
            location.hash = "#loginSection";
            return;
        } else {
            let storageKey = localStorage.getItem("email");
            sessionStorage.setItem("email", storageKey);
            sessionStorage.setItem("hash", storageValue);
            toastr.success("welcome");
            location.hash = "#homeSection";
        }
        return;
    } else {
        toastr.success("welcome");
    }
    return;
}

if (sessionValue == null || sessionValue == "") {
    if (storageValue == null || storageValue == "") {
        let tempLocationHash = window.location.hash;
        if (tempLocationHash !== "#registrationSection" || tempLocationHash !== "#loginSection") {
            sessionCheck();
        }
    }
}

for (var i = nl.length; i--; allPages.unshift(nl[i]));
$(document).ready(function () {
    window.location.hash = "";
    window.location.hash = "#homeSection";


});

function hashLocationSelector(pageHash) {
    let Pages = [];
    for (let i = 0; i < allPages.length; i++) {
        let tempElement = allPages[i];
        if (tempElement.id === pageHash.substring(1)) {
            $(allPages[i]).show();
            window.location.hash = pageHash;
        } else {
            Pages.push(allPages[i]);
        }
    }
    pagesDisplayNone(Pages);

}
//reducedPageHashes sind alle .page ohne den Zielhash
function pagesDisplayNone(reducedPageHashes) {

    for (var i = 0; i < reducedPageHashes.length; i++) {
        $(reducedPageHashes[i]).hide();
    }

}



$(window).on("hashchange", function (event) {
    currentHash = window.location.hash;
    let emptyStorageStatus = false;
    if (localStorage.getItem("hash") === null && sessionStorage.getItem("hash") === null) {
        if (currentHash === "#registrationSection") {
            emptyStorageStatus = true;
            hashLocationSelector("#registrationSection");

        } else {
            emptyStorageStatus = true;
            currentHash = "#loginSection";
            return event.preventDefault();
        }
    }
    if (!allPages.indexOf(currentHash) || emptyStorageStatus) {
        if (currentHash === "#registrationSection") {            
            return hashLocationSelector("#registrationSection");
        }
        return hashLocationSelector("#loginSection");
    } else {
        let locationToChangeTo = window.location.hash;
        switch (locationToChangeTo) {
            case "#homeSection":
                hashLocationSelector(locationToChangeTo);
                break;
            case "#registrationSection":
                hashLocationSelector(locationToChangeTo);
                break;
            case "#loginSection":
                hashLocationSelector(locationToChangeTo);
                break;
        }
    }

});

//------------------------------------------------- /hashChange und PageSelector------------------------------------------------------------

//---------------------------------------------------------- Register ----------------------------------------------------------------------


let regDoButton = $('#regDoButton');
let regDoBInput = $('#regDoBInput');
let mydatepicker = regDoBInput.datepicker({ showEvent: 'none' }).data('datepicker');

function mydatePickerShow() {
    $(regDoBInput).datepicker("show");
    regDoBInput.focus();
}



function registerSubmit() {
    let regEmail = document.getElementById("regEmailInput");
    let regPass1 = document.getElementById("regPass1Input");
    let regPass2 = document.getElementById("regPass2Input");

}



//---------------------------------------------------------- /Register ----------------------------------------------------------------------



//---------------------------------------------------------- Login ----------------------------------------------------------------------



function loginSubmit() {
    var memail = document.getElementById('emailInput').value;
    var mpassword = document.getElementById('passInput').value;

    if (memail == "" || mpassword == "") {
        toastr.info("Die Felder müssen ausgefüllt sein");
        return;
    } else {
        var fromdata = {
            email: memail,
            password: mpassword

        };
        fetch('https://localhost:44302/Home/Login', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ fromdata })

        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            if (data.status === "success") {
                console.log(data);
                location.hash = "#homeSection";
                localStorage.setItem("email", memail);
                localStorage.setItem("hash", data.Hash);
                sessionStorage.setItem("email", memail);
                sessionStorage.setItem("hash", data.Hash);
                toastr.success("Sie sind dauerhaft eingeloggt");

            } else {
                toastr.error(
                    "Diese Mail-Passwort Kombination wurde nicht gefunden:\n" +
                    data.value
                );
                return data.value;
            }

        }).catch(function (err) {
            console.log(err);
        });
    }
}

window.addEventListener('beforeinstallprompt', function (e) {
    console.log('beforeinstallprompt Event fired');
    e.preventDefault();

    // Stash the event so it can be triggered later.
    deferredPrompt = e;

    return false;
});

button1.addEventListener('click', function () {
    if (deferredPrompt !== undefined) {
        // The user has had a postive interaction with our app and Chrome
        // has tried to prompt previously, so let's show the prompt.
        deferredPrompt.prompt();

        // Follow what the user has done with the prompt.
        deferredPrompt.userChoice.then(function (choiceResult) {

            console.log(choiceResult.outcome);

            if (choiceResult.outcome == 'dismissed') {
                console.log('User cancelled home screen install');
            }
            else {
                console.log('User added to home screen');
            }

            // We no longer need the prompt.  Clear it up.
            deferredPrompt = null;
        });
    }
});
