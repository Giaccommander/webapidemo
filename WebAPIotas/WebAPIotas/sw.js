﻿self.addEventListener('install', function (event) {
    event.waitUntil(caches.open('static')
        .then(function (cache) {
            cache.addAll([
                '/',
                'Home/Index',
                '/favicon.ico',
                'Scripts/jquery-3.3.1.min.js',
                'Scripts/umd/popper.min.js',
                'Scripts/bootstrap.min.js',
                'Scripts/toastr.min.js',
                'Content/bootstrap.min.css',
                'Content/toastr.min.css',
                'Scripts/app.js',
                'Content/indexPWA.css',
                '/offline.html'

            ]);
        })
    );
});





self.addEventListener('fetch', function (event) {
    var request = event.request;
    event.respondWith(
        caches.match(request)
            .then(function (response) {
                if (response) {
                    return response;
                } else {
                    return fetch(request);
                }
            }).catch(function (err) {
                console.log("[ServiceWorker] Ein Fehler ist aufgetreten: " + err);
                return caches.open('static')
                    .then(function (cache) {
                        return cache.match('offline.html');
                    });
            })
    );

});



