var stationenAnker = document.querySelector('#nav-tabContent');
var dataParent ="";
var stationenAnker2 = document.querySelector('#list-tab');

//Die Google Maps Iframes sind einfache "In HTML einbetten" Ansichten und müssen für jede Station inkl. LatLong eingefügt werden
//LatLong muss exact mit den Datenbank-koordinaten übereinstimmen da sonst nur eine Karte von Österreich angezeigt wird
// { coordinate: LatLong,
//   mapsEmbeded : "Hier nur der https:// LINK"  }
var iFrameUrl = [{
        "coordinate": "47.7359375,13.050437500000044",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2683.339732174674!2d13.048425415977487!3d47.73606267919339!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4776915ee3d6134d%3A0x17a4166cacc7ff58!2sBP+Tankstelle+Leikermoser+Energiehandel+GmbH!5e0!3m2!1sde!2sat!4v1549276772942"
    },
    {
        "coordinate": "47.78568749999999,12.975812499999961",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2680.7794202140626!2d12.973935115978975!3d47.78572507919697!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47768fcd89e5531f%3A0x164f2966057d20cf!2sBP+Tankstelle+Leikermoser+Energiehandel+GmbH!5e0!3m2!1sde!2sat!4v1549281159628"
    },
    {
        "coordinate": "47.3509375,13.194062499999973",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d168.94533300285835!2d13.194084448894499!3d47.35095105680649!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4776d830f196c193%3A0x59ce82cb9bfa5b66!2sBP!5e0!3m2!1sde!2sat!4v1549287758090"
    },
    {
        "coordinate": "47.3343125,13.189312500000028",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d168.99863181943132!2d13.189306464184334!3d47.33429883172612!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4776d8171a7b8beb%3A0xb95bc0362655073b!2sBP+Tankstelle+Leikermoser+Energiehandel+GmbH!5e0!3m2!1sde!2sat!4v1549287905059"
    },
    {
        "coordinate": "47.6653125,12.752812500000005",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d548.7881130117163!2d12.752528881523283!3d47.66530491962191!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4776f515db86c11d%3A0x6372b343f1218e8!2sBP+Tankstelle+Leikermoser+Energiehandel+GmbH!5e0!3m2!1sde!2sat!4v1549288099482"
    },
    {
        "coordinate": "47.0954375,13.63868750000006",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1358.0914515228037!2d13.637716542005355!3d47.09548494209821!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4770c35aa05bc573%3A0xceb1b7e6128d69b9!2sBP!5e0!3m2!1sde!2sat!4v1549288215131"
    },
    {
        "coordinate": "47.9058125,13.193562499999985",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1337.2886440812317!2d13.192553991288769!3d47.90586640784948!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47769ee57ae90d75%3A0xd8b94f020e13c9f6!2slm-energy!5e0!3m2!1sde!2sat!4v1549288297898"
    },
    {
        "coordinate": "47.7313125,13.064187500000003",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1341.7903080518843!2d13.063200450049726!3d47.731388237945794!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x477693fe07cdec21%3A0x4269d2bba3ffb2f2!2sLMEnergy+Anif-Niederalm!5e0!3m2!1sde!2sat!4v1549288375633"
    },
    {
        "coordinate": "47.8315625,13.085562500000037",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1339.2049351939093!2d13.084741748707046!3d47.831652493131415!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47769a4197fb638f%3A0x3d24819045008751!2sLMEnergy!5e0!3m2!1sde!2sat!4v1549288433570"
    },
    {
        "coordinate": "47.7850625,13.026562499999955",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1340.4046592815112!2d13.025693468264224!3d47.78514536600965!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4776904fbc5e5c5d%3A0xce2781bfd6ff1312!2sLMEnergy+Tankstelle!5e0!3m2!1sde!2sat!4v1549288526641"
    },
    {
        "coordinate": "47.78568749999999,12.975812499999961",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2680.7794202140626!2d12.973935115978975!3d47.78572507919697!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47768fcd89e5531f%3A0x164f2966057d20cf!2sBP+Tankstelle+Leikermoser+Energiehandel+GmbH!5e0!3m2!1sde!2sat!4v1549281159628"
    },
    {
        "coordinate": "47.5649375,13.342062499999997",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1346.0727998119755!2d13.34107602125871!3d47.56495547348253!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDfCsDMzJzUzLjgiTiAxM8KwMjAnMzEuNCJF!5e0!3m2!1sde!2sat!4v1549615808798"
    },
    {
        "coordinate": "47.2813125,12.575062500000058",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d569.0133978590931!2d12.574829639180654!3d47.281135796030775!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4777071320f90e99%3A0xff47d31c339cbb83!2sLMEnergy!5e0!3m2!1sde!2sat!4v1549616090525"
    },
    {
        "coordinate": "47.4334375,13.51931250000007",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d674.7270002507726!2d13.518999273587314!3d47.433237225735475!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDfCsDI2JzAwLjQiTiAxM8KwMzEnMDkuNSJF!5e0!3m2!1sde!2sat!4v1549616243548"
    },
    {
        "coordinate": "47.9038125,13.12043749999998",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d562.2834459703828!2d13.120372947091404!3d47.90376448160478!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDfCsDU0JzEzLjciTiAxM8KwMDcnMTMuNiJF!5e0!3m2!1sde!2sat!4v1549617863786"
    },
    {
        "coordinate": "47.2609375,11.421437500000025",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d676.9351793880427!2d11.420825950921545!3d47.26077317195182!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479d6946355d24bf%3A0x9c5249dc6e05ff6f!2sBP!5e0!3m2!1sde!2sat!4v1549617989274"
    },
    {
        "coordinate": "47.2738125,11.428187500000035",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d957.0971475457088!2d11.428524608682581!3d47.27369977896945!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDfCsDE2JzI1LjciTiAxMcKwMjUnNDEuNSJF!5e0!3m2!1sde!2sat!4v1549619294276"
    },
    {
        "coordinate": "47.1410625,9.833687499999996",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d678.464942015643!2d9.83351872723573!3d47.14101334397997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479b4e2c44541bef%3A0x7910d07a3d20ced8!2sBP!5e0!3m2!1sde!2sat!4v1549619385977"
    },
    {
        "coordinate": "47.9880625,13.249437499999999",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1122.7440146686768!2d13.24886800023761!3d47.98761621240277!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDfCsDU5JzE3LjAiTiAxM8KwMTQnNTguMCJF!5e0!3m2!1sde!2sat!4v1549619444253"
    },
    {
        "coordinate": "47.0163125,9.99131250000005",
        "mapsEmbeded": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d480.871870590644!2d9.991512827957258!3d47.01624558018384!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479b555412bb2bdd%3A0x7912b53e930ee5ce!2sBP!5e0!3m2!1sde!2sat!4v1549619527639"
    }
];



function createStationen(tempAddress, tempLat, tempLong, tempLatLong, tempDescriptionText, tempCardTitleText, activeClassname, showactiveClass, tempTelNr) {
    // tabPaneClassname entweder 'tab-pane' oder 'tab-pane fade show active' darauf muss beim erzeugen Rücksicht genommen werden
    var tempiFrameUrl = "";
    dataParent = 'list-tab';

    function findLatLong(coordi) {

        return coordi.coordinate === tempLatLong;

    }
    if (iFrameUrl.find(findLatLong) == undefined || iFrameUrl.find(findLatLong) == false) {
        tempiFrameUrl = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1375058.1671504832!2d12.22481345878588!3d47.69101323143481!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x476d079b259d2a7f%3A0x1012d47bdde4c1af!2s%C3%96sterreich!5e0!3m2!1sde!2sat!4v1549288935242";
    } else {
        tempiFrameUrl = iFrameUrl.find(findLatLong).mapsEmbeded;
    }

    var tabPane = document.createElement('div');
    tabPane.className = 'tab-pane fade ' + showactiveClass; //Hier einmal 'active' nicht vergessen
    $(tabPane).attr({
        "id": "list-home" + i,
        "role": "tabpanel",
        "aria-labelledby": "list-home" + i + "-list"
    });
    stationenAnker.appendChild(tabPane);
    var card = document.createElement('div');
    card.className = 'card';
    tabPane.appendChild(card);
    var cardImages = document.createElement('div');
    cardImages.className = 'cardImages';
    card.appendChild(cardImages);
    var myMaps = document.createElement('div');
    myMaps.className = 'card-img-top';
    $(myMaps).attr({
        "id": "Mymap" + i,
        "alt": "Card image cap"
    });
    cardImages.appendChild(myMaps);
    $("#Mymap" + i).append('<iframe src="' + tempiFrameUrl + '" width="370" height="280" frameborder="0" style="border:0" allowfullscreen></iframe>');
    var cardbody = document.createElement('div');
    cardbody.className = 'card-body';
    card.appendChild(cardbody);
    var button = document.createElement('button');
    button.className = 'btn btn-outline-info pull-right';
    button.textContent = 'Route';
    $(button).attr({
        "type": "button",
        "onclick": "myNavFunc(" + tempLat + "," + tempLong + ")" //Hier googlemaps lat und long für Routenbutton
    });
    cardbody.appendChild(button);
    var cardTitle = document.createElement('h4');
    cardTitle.className = 'card-title';
    cardTitle.textContent = tempDescriptionText;
    cardbody.appendChild(cardTitle);
    var hrLight = document.createElement('hr');
    hrLight.className = 'hr-light';
    cardbody.appendChild(hrLight);
    var p1 = document.createElement('p');
    p1.className = 'card-text';
    p1.textContent = tempAddress; //ADRESSE HIER
    cardbody.appendChild(p1);
    var p2 = document.createElement('p');
    p2.textContent = tempTelNr;
    cardbody.appendChild(p2);
    var roundedBottom = document.createElement('div');
    roundedBottom.className = 'rounded-bottom mdb-color lighten-3 text-center pt-3';
    card.appendChild(roundedBottom);
    var ul = document.createElement('ul');
    ul.className = 'list-unstyled list-inline font-small';
    roundedBottom.appendChild(ul);
    var li1 = document.createElement('li');
    li1.className = 'list-inline-item pr-2 white-text';
    ul.appendChild(li1);
    var i1 = document.createElement('i');
    i1.className = 'fa fa-clock-o pr-1';
    i1.textContent = " " + dateToday();
    li1.appendChild(i1);

    function dateToday() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //jänner ist 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        today = dd + '/' + mm + '/' + yyyy;
        return today;
    }

    // var li2 = document.createElement('li');
    // li2.className = 'list-inline-item pr-2';
    // var i2 = document.createElement('i');
    // i2.className = 'fa fa-comments-o pr-1';
    // li2.appendChild(i2);
    // li2.textContent = '12';  12 comments on socialMedia
    // ul.appendChild(li2);

    // var li3 = document.createElement('li');
    // li3.className = 'list-inline-item pr-2';
    // var i3 = document.createElement('i');
    // i3.className = 'fa fa-facebook pr-1';  
    // li3.appendChild(i3);
    // li3.textContent = '21';         21 comments
    // ul.appendChild(li3);
    // var li4 = document.createElement('li');
    // li4.className = 'list-inline-item';
    // var i4 = document.createElement('i');
    // i4.className = 'fa fa-twitter pr-1';
    // li4.appendChild(i4);
    // li4.textContent = '5';              
    // ul.appendChild(li4);


    var a1 = document.createElement('a');
    a1.className = 'list-group-item list-group-item-action' + activeClassname; //Hier einmal 'active' nicht vergessen
    $(a1).attr({
        "id": "list-home" + i + "-list",
        "data-toggle": "list",
        "href": "#list-home" + i,
        "role": "tab",
        "aria-controls": "home" + i
    });
    a1.textContent = tempAddress;
    stationenAnker2.appendChild(a1);
    var hrLight1 = document.createElement('hr');
    hrLight1.className = 'hr-light';
    a1.appendChild(hrLight1);
    var p3 = document.createElement('p');
    p3.className = 'small text-left';
    p3.textContent = tempTelNr; 
    a1.appendChild(p3);

    return i++;
}