//Support für ältere Browser ohne fetch und promise 
if (!window.Promise) {
    window.Promise = Promise;
}

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js').then(function () {
        console.log('ServiceWorker registered');
    });
}




window.addEventListener('beforeinstallprompt', function (event) {
    console.log('beforeinstallprompt fired');
    event.preventDefault();
    deferredPrompt = event;
    return false;
});
var sessionValue = sessionStorage.getItem('hash');
var sessionKey = sessionStorage.getItem('email');
var storageValue = localStorage.getItem('hash');
var storagKey = localStorage.getItem('email');
document.addEventListener("DOMContentLoaded", function () {
    try {
        //  sessionValue = sessionStorage.getItem('hash');
        //  storageValue = localStorage.getItem('hash');
        sessionCheck();
    } catch (error) {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "200",
            "hideDuration": "500",
            "timeOut": 0,
            "extendedTimeOut": 0,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        };
        toastr.error(error + '<br /><br /><button type="button" class="btn clear">Damn you</button>', "Shit happened");
    }
});

// From your client pages:
var channel = new BroadcastChannel('sw-messages');
channel.addEventListener('message', function (event) {
    var messa = event.data;
    document.getElementById("swVersion").innerHTML = messa.title;
});

// function configPushSubscript() {
//     if (!('servicWorker' in navigator)) {
//         return;
//     }

//     navigator.serviceWorker.ready
//         .then(function (swreg) {
//             return swreg.pushManager.getSubscription();
//         }).then(function (sub) {
//             if (sub === null) {

//             } else {

//             }
//         });

// }






// navbar collapser bei Berührung wird die Navbar wieder geschlossen
$('.navbar-collapse a').click(function () {
    $(".navbar-collapse").collapse('hide');
});

$(document).click(function (e) {
    if (!$(e.target).is('.navbar-collapse a')) {
        $('.navbar-collapse').collapse('hide');
    }
});

//Navbar wird auf loginSection und registerSection nicht gezeigt. Auf allen anderen Sectionen/Pages sollte die Navbar sichtbar sein.
function showPageNonav(pageId) {
    $(".page").hide();
    $(pageId).show();
    $(".navbar").hide();
    location.hash = pageId;



}
location.hash = "";
location.hash = "#loginSection";


function showPage(pageId) {
    $(".page").hide();
    $(pageId).show();
    $(".navbar").show();
    location.hash = pageId;

}
location.hash = "";
location.hash = "#loginSection";

$(window).on("hashchange", function (event) {
    if (location.hash == "#registerSection" || location.hash == "#loginSection" || location.hash == "#iForgotPassword") {
        showPageNonav(location.hash);
    } else {
        showPage(location.hash);
    }

    $(".nav-link", "btn").each(function () {
        if ($(this).attr("href") || $(this).attr("onclick") === location.hash) {
            $(this).addClass("selected");
            // showPage(location.hash);
        } else {
            $(this).removeClass("selected");
        }
    });
});






// -------------------------Registrierung-----------------------------------------------------





// regEx für passwort 1groß,1nummer,1zeichen,mindestens 8, maximal 20
function CheckPassword() {
    
    var registerPasswort = document.getElementById('registerPasswort').value;
    var registerPasswort2 = document.getElementById('registerPasswort2').value;

    var regEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,20}$/;
    if (registerPasswort.match(regEx)) {

        if (registerPasswort != registerPasswort2) {
            toastr.warning("passwörter sind nicht identisch");
            return false;
        } else {
            SendRegPostForm();
            return;
        }
    } else {
        toastr.info('Mindestens 7 Zeichen,<br/> 1 Großbuchstabe,<br/> 1 Sonderzeichen,<br/> 1 Zahl');
        return false;
    }
}



function SendRegPostForm() {
    var firstname = document.getElementById("registerVorname").value;
    var lastname = document.getElementById("registerNachname").value;
    var email = document.getElementById("registerEmail").value;
    var password = document.getElementById('registerPasswort').value;
    var encryptedpassword = null;
    var deliverCustomerId = null;
    var salt = null;
    var IsActivated = null;

    var tempUser = {
        "deliverCustomerId": deliverCustomerId,
        "email": email,
        "firstname": firstname,
        "lastname": lastname,
        "password": password,
        "encryptedpassword": encryptedpassword,
        "salt": salt,
        "IsActivated": IsActivated
    };
    fetch('http://tankstelle.otas.at:8500/user/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(tempUser)
        }).then(function (response) {
            // console.log(response);
            return response.json();
        }).then(function (data) {
            if (data.successful == true) {
                toastr.success("Jetzt musst du nur noch die Email bestätigen.<br/>Alexa ! erinnere mich heute Champgner zu kaufen!", "BOTas sagt: ");
                return showPageNonav('#loginSection');
            } else {
                toastr.warning("Die DatenBank sagt Nein...!?!", "BOTas sagt: ");
                console.log(data + " " + "Da ist was schief gegangen");
            }

        })
        .catch(function (err) {
            toastr.error("Der Server ist gerade EXPLODIERT, Sorry!", "BOTas sagt: ");
            console.log(err);
        });


}

// --------------Login----------------------------------------------------------------------
var voucherReady = false;

function sessionCheck() {
    if (sessionValue == null || sessionValue == "") {
        if (storageValue == null || storageValue == "") {
            return;
        } else {
            voucherReady = true;
            sessionValue = storageValue;
            sessionKey = storagKey;
            toastr.options = {
                "positionClass": "toast-bottom-full-width",
                "timeOut": "2000"
            };
            toastr.options.hideMethod = 'slideUp';
            toastr.success("welcome");
            getUserData(storageValue);
        }
        return;
    } else {
        voucherReady = true;
        toastr.options = {
            "positionClass": "toast-bottom-full-width",
            "timeOut": "2000"
        };
        toastr.options.hideMethod = 'slideUp';
        toastr.success("welcome");
        getUserData(sessionValue);
    }
    return;
}


function sendLoginForm() {

    var loginEmail = document.getElementById('loginName').value;
    var loginPassword = document.getElementById('loginPasswort').value;
    var stayLogged = document.getElementById('stayLogged');

    var loginTempUser = {
        "email": loginEmail,
        "password": loginPassword
    };

    fetch('http://tankstelle.otas.at:8500/app/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(loginTempUser)
        }).then(function (response) {

            return response.json();
        }).then(function (data) {
            var loginData = data;
            if (loginData.successful == true) {
                voucherReady = true;
                if (stayLogged.checked == true) {
                    localStorage.setItem('email', loginData.email);
                    localStorage.setItem('hash', loginData.sessionid);
                    sessionStorage.setItem('email', loginData.email);
                    sessionStorage.setItem('hash', loginData.sessionid);
                    toastr.success("Sie sind dauerhaft eingeloggt");
                } else {
                    sessionStorage.setItem('email', loginData.email);
                    sessionStorage.setItem('hash', loginData.sessionid);
                    toastr.success("Sie sind eingeloggt");
                }
                // console.log(sessionStorage.getItem('hash') + sessionStorage.getItem('email'));

                getUserData(loginData.sessionid);
            } else {

                alert(data.message);
                return data.message;
            }
        })
        .catch(function (err) {
            console.log("Hier ist ein fehler: " + err);
        });
}


function getUserData(sessionid) {

    if (voucherReady) {

        fetch('http://tankstelle.otas.at:8500/app/getuserdata', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(sessionid)
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            console.log(data);
            document.getElementById("karteName").innerHTML = data.firstname + " " + data.lastname;
            document.getElementById("karteNummer").innerHTML = "Nr.: " + data.cardNo;
            // if (data.plate == "") {
            //     data.plate = "NoPlate";
            // }
            document.getElementById("plate").innerHTML = data.plate;
            var qrAnker = document.querySelector('#qrAnker');
            var qrCodeHome = document.createElement('div');
            $(qrCodeHome).attr({
                "id": "qrcodeHome",
                "style": "width:100px"
            });
            qrAnker.appendChild(qrCodeHome);
            jQuery('#qrcodeHome').qrcode({
                text: data.track2,
                ecLevel: 'H',
                size: 100
            });
            // console.log(data);
        }).catch(function (err) {
            console.log("Hier ist ein fehler: " + err);
        });
    }
    showPage("homeSection");
}

// function reloadIcon() {
//     var spiner = document.getElementById('gutscheinSyncButton');
//     $(spiner).addClass('fa-spin');
//     gutscheinNavigator();
//     setTimeout(function () {
//         $(spiner).removeClass('fa-spin');
//     }, 3000);
// }

function reloadIcon() {
    var spiner = document.getElementById('gutscheinSyncButton');
    $(spiner).addClass('refreshing');
    gutscheinNavigator();
    setTimeout(function () {
        $(spiner).removeClass('refreshing');
    }, 2000);
}

function gutscheinNavigator() {
    var emptyGutscheineLeeren = document.getElementById('emptyVoucherShow');
    var gutscheineLeeren = document.getElementById('accordionListe');

    var emptyRedeemedGutscheineLeeren = document.getElementById('emptyRedeemedVoucherShow');
    var redeemedgutscheineLeeren = document.getElementById('accordionListeALT');

    if (emptyRedeemedGutscheineLeeren.firstChild != null) {
        while (emptyRedeemedGutscheineLeeren.firstChild) {
            emptyRedeemedGutscheineLeeren.removeChild(emptyRedeemedGutscheineLeeren.firstChild);
        }
    } else {

        while (redeemedgutscheineLeeren.firstChild) {
            redeemedgutscheineLeeren.removeChild(redeemedgutscheineLeeren.firstChild);
        }
    }

    if (emptyGutscheineLeeren.firstChild != null) {
        while (emptyGutscheineLeeren.firstChild) {
            emptyGutscheineLeeren.removeChild(emptyGutscheineLeeren.firstChild);
        }
    } else {

        while (gutscheineLeeren.firstChild) {
            gutscheineLeeren.removeChild(gutscheineLeeren.firstChild);
        }
    }
    var sessionid = sessionStorage.getItem('hash');
    getVocher(sessionid);
}



function getVocher(sessionid) {
    if (voucherReady) {

        fetch('http://tankstelle.otas.at:8500/app/voucherlist', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(sessionid)
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            console.log(data.VoucherCollection);
            voucherLoop(data);
        }).catch(function (err) {
            console.log("Hier ist ein fehler: " + err);
        });

    }
}

function voucherLoop(vouchers) {
    var vouchercnt = 0;
    var redeemedVoucher = [];
    gutscheine = vouchers;
    console.log(vouchers);
    if (gutscheine.VoucherCollection.length == 0 || gutscheine.VoucherCollection.length == null) {
        emptyRedeemedVoucherShow();
        emptyVoucherShow();

    } else {
        for (var i = 0; i < gutscheine.VoucherCollection.length; i++) {
            if (gutscheine.VoucherCollection[i].DateRedeemed != null) {
                redeemedVoucher.push(gutscheine.VoucherCollection[i]);
                continue;
            }
            var cardQrcode = gutscheine.VoucherCollection[i].VoucherId;
            // var cardStartDate = gutscheine.VoucherCollection[i].startDate;
            var cardExpireDate = gutscheine.VoucherCollection[i].VoucherExpiry;
            // var cardJSBarcode = gutscheine.VoucherCollection[i].VoucherId;
            var cardText = gutscheine.VoucherCollection[i].ConsecutiveVoucherNo;
            var cardDescription = gutscheine.VoucherCollection[i].ItemDescription;
            var cardValue;
            if (gutscheine.VoucherCollection[i].VoucherValue == null) {
                cardValue = " ";
            } else {
                cardValue = gutscheine.VoucherCollection[i].VoucherValue;
            }
            var bg = "#d9db00";
            var accordionselector = true;
            // console.log(cardQrcode+cardStartDate+cardExpireDate+cardJSBarcode+cardText+cardText+cardValue);
            createCard(cardQrcode, cardExpireDate, cardText, cardDescription, cardValue, bg, accordionselector);
            vouchercnt++;

        }

        if (redeemedVoucher.length != 0) {
            i = 0;
            for (i = 0; i < redeemedVoucher.length; i++) {
                var deemedcardQrcode = redeemedVoucher[i].VoucherId;
                // var deemedcardStartDate = redeemedVoucher[i].startDate;
                var deemedcardExpireDate = redeemedVoucher[i].VoucherExpiry;
                // var deemedcardJSBarcode = redeemedVoucher[i].VoucherId;
                var deemedcardText = redeemedVoucher[i].ConsecutiveVoucherNo;
                var deemedcardDescription = redeemedVoucher[i].ItemDescription;
                var deemedcardValue;
                if (redeemedVoucher[i].VoucherValue == null) {
                    deemedcardValue = " ";
                } else {
                    deemedcardValue = redeemedVoucher[i].VoucherValue;
                }
                var deemedbg = "#706d6d";
                var deemedaccordionSelector = false;
                // console.log(cardQrcode+cardStartDate+cardExpireDate+cardJSBarcode+cardText+cardText+cardValue);
                createCard(deemedcardQrcode, deemedcardExpireDate, deemedcardText, deemedcardDescription, deemedcardValue, deemedbg, deemedaccordionSelector);
            }
        } else {
            emptyRedeemedVoucherShow();
        }
    }
    if (vouchercnt === 0) {
        emptyVoucherShow();
    }
}

function emptyVoucherShow() {
    var emptyVocherAnker = document.getElementById('emptyVoucherShow');
    var emptyVoucherDiv1 = document.createElement('div'); //div1
    emptyVoucherDiv1.className = 'text-centered';
    var emptyVoucherDiv2 = document.createElement('div'); //div2
    emptyVoucherDiv2.className = 'text-centered';
    emptyVoucherDiv2.textContent = 'Sie haben zur Zeit leider keine Gutscheine... aber hoffentlich bald';
    $(emptyVoucherDiv2).attr({
        "id": "emptyVoucherId"
    });
    emptyVoucherDiv1.appendChild(emptyVoucherDiv2);
    emptyVocherAnker.appendChild(emptyVoucherDiv1);
    vouchercnt++;

}

function emptyRedeemedVoucherShow() {
    var trennLinie = document.getElementById('gutscheineTrennlinie');
    trennLinie.style.display = 'none';
    var emptyRedeemedVocherAnker = document.getElementById('emptyRedeemedVoucherShow');
    var emptyRedeemedVoucherDiv1 = document.createElement('div'); //div1
    emptyRedeemedVoucherDiv1.className = 'text-centered';
    var emptyRedeemedVoucherDiv2 = document.createElement('div'); //div2
    emptyRedeemedVoucherDiv2.className = 'text-centered';
    emptyRedeemedVoucherDiv2.textContent = 'Sie haben noch keine Gutscheine verbraucht.';
    $(emptyRedeemedVoucherDiv2).attr({
        "id": "emptyRedeemedVoucherId"
    });
    emptyRedeemedVoucherDiv1.appendChild(emptyRedeemedVoucherDiv2);
    emptyRedeemedVocherAnker.appendChild(emptyRedeemedVoucherDiv1);
}

// ---------------------Logout-----------------------------------------

function logout() {

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-full-width",
        "preventDuplicates": true,
        "showDuration": "200",
        "hideDuration": "500",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr.info('Alle Log-Cookies werden weggefressen', 'CookieMonster sagt:');
    if (sessionStorage != null || localStorage != null) {
        var logoutsessionValue = sessionStorage.getItem('hash');
        fetch('http://tankstelle.otas.at:8500/app/logout', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(logoutsessionValue)
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            if (data.successful == true) {
                sessionStorage.removeItem('email', 'value');
                sessionStorage.removeItem('hash', 'value');
                localStorage.removeItem('email');
                localStorage.removeItem('hash');
                var emptyQrAnker = document.getElementById('qrAnker');
                emptyQrAnker.removeChild(emptyQrAnker.firstChild);
            }
            // showPageNonav('#loginSection');
        }).catch(function (err) {
            sessionStorage.removeItem('email', 'value');
            sessionStorage.removeItem('hash', 'value');
            localStorage.removeItem('email');
            localStorage.removeItem('hash');
            var emptyQrAnker = document.getElementById('qrAnker');
            emptyQrAnker.removeChild(emptyQrAnker.firstChild);
            console.log("Hier ist ein fehler: " + err);
        });
        return showPageNonav('#loginSection');
    }
}





// Gutschein Zentrierung der CollapseKarten

$(document).on("shown.bs.collapse", ".collapse", function (e) {
    var $card = $(this).closest('.card');
    $('html,body').animate({
        scrollTop: $card.offset().top - 70
    }, 100);
});

// show.bs.collaps -> bevor aufgeklappt wird
// shown(!N).bs.collaps -> nachdem aufgeklappt wurde