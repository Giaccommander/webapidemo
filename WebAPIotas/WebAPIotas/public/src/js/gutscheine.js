var gutscheinArea = document.querySelector('#accordionListe');
var dataParent;


function createCard(tempQrCode, tempExpireDate, tempText, tempDescription, tempValue, tempbg, tempArea,tempDatumText) {
  if (tempArea == false) {
    gutscheinArea = document.querySelector('#accordionListeALT');
    dataParent = "accordionListeALT";
  }else{
    dataParent = "accordionListe";
  }
  var cardZdeth0bordered = document.createElement('div'); //div
  cardZdeth0bordered.className = 'card z-depth-0 bordered';
  var cardHeader = document.createElement('div');
  cardHeader.className = 'card-header';
  $(cardHeader).attr({
    "id": "heading" + [i]
  });
  cardHeader.style.backgroundColor = tempbg;
  cardZdeth0bordered.appendChild(cardHeader);
  var mb0 = document.createElement('h5'); //h5
  mb0.className = 'mb-0';
  cardHeader.appendChild(mb0);
  var btnbtnlink = document.createElement('button'); //button
  btnbtnlink.className = 'btn btn-link waves-effect waves-light collapsed';
  $(btnbtnlink).attr({
    "type": "button",
    "data-toggle": "collapse",
    "data-target": "#Gutschein" + [i],
    "aria-expanded": "false",
    "aria-controls": "Gutschein" + [i],
  });
  mb0.appendChild(btnbtnlink);
  var spanPullLeft = document.createElement('span');
  spanPullLeft.className = 'fa-pull-left';
  spanPullLeft.textContent = tempDescription;
  btnbtnlink.appendChild(spanPullLeft);
  var icon = document.createElement('i');
  icon.className = 'fa fa-angle-down fa-lg ml-5 fa-pull-right rotate';
  $(icon).attr({
    "arial-hidden": "true"
  });
  btnbtnlink.appendChild(icon);

  var collapseShow = document.createElement('div');
  collapseShow.className = 'collapse';
  $(collapseShow).attr({
    "id": "Gutschein" + [i],
    "aria-labeledby": "Gutschein" + [i],
    "data-parent": "#"+dataParent
  });
  cardZdeth0bordered.appendChild(collapseShow);
  var cardBody = document.createElement('div');
  cardBody.className = 'card-body';
  collapseShow.appendChild(cardBody);
  var cardBodyContainer = document.createElement('div');
  cardBodyContainer.className = 'container';
  $(cardBodyContainer).attr({
    "id": "gutscheinQr"
  });
  cardBody.appendChild(cardBodyContainer);
  var qrCode = document.createElement('div');
  $(qrCode).attr({
    "id": "qrcode" + [i],
    "style": "width:100px"
  });
  cardBodyContainer.appendChild(qrCode);
  var cardHr = document.createElement('hr');
  cardBody.appendChild(cardHr);
  var cardP = document.createElement('p');
  cardP.textContent = tempValue + " " + tempText;
  cardBody.appendChild(cardP);
  var cardBodyContainer2 = document.createElement("div");
  cardBodyContainer2.className = 'container';
  var cardRow = document.createElement('div');
  cardRow.className = 'row';
  cardBodyContainer2.appendChild(cardRow);
  // var colSm5 = document.createElement('div'); 
  // colSm5.className = 'col-sm-5';
  // cardRow.appendChild(colSm5);
  // var col1P = document.createElement('p');
  // col1P.textContent = 'Von: ' + tempStartDate;
  // colSm5.appendChild(col1P);
  var colSm52 = document.createElement('div');
  colSm52.className = 'col-sm-12';
  cardRow.appendChild(colSm52);
  var col2P = document.createElement('p');
  col2P.textContent = tempDatumText + tempExpireDate;
  colSm52.appendChild(col2P);
  cardBody.appendChild(cardBodyContainer2);
  // var cardHr2 = document.createElement('hr');
  // cardBody.appendChild(cardHr2);
  // var cardBodyContainer3 = document.createElement('div');
  // cardBodyContainer3.className = 'container';
  // $(cardBodyContainer3).attr({
  //   "id": "gutscheinBarcode"
  // });
  // cardBody.appendChild(cardBodyContainer3);
  // var svg = document.createElement('div');
  // $(svg).attr({
  //   "id": "barcode" + [i],
  // });

  // cardBodyContainer3.appendChild(svg);
  collapseShow.appendChild(cardBody);
  gutscheinArea.appendChild(cardZdeth0bordered);
  jQuery('#qrcode' + [i]).qrcode({
    text: tempQrCode,
    ecLevel: 'H',
    size: 100
  });
  dataParent = "accordionListe";
  gutscheinArea = document.querySelector('#accordionListe');
  return i++;
}