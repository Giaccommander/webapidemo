var url = window.location.href;
var urlString = "http://tankstelle.otas.at:8500/user/activate/";
// http://192.168.1.179:8500/user/activate/12581fb9-0d7c-4e4d-a92a-8bc7c262eb85
// 12581fb9-0d7c-4e4d-a92a-8bc7c262eb85 wird versendet
var theUrl = "";
var payUnityId = "";
window.onload = parseURLParams(window.location.href);
var br = "\r\n";

var localHosting = "http://localhost:8080";
var iisHosting = "http://tankstelle.otas.at";
var iisSHosting = "https://tankstelle.otas.at";
var Hosting = iisHosting; //Hier bitte passende Addresse einfügen
function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1,
        queryEnd = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/%2F/g, "\+");
    pairs = query.replace(/\+/g, " ").split("&");
    parms = {};
    var n, v, nv;

    if (query === url || query === "") return;
    //für url´s die key und value senden z.b. http://blabla.at/user/activate?hash=blabla "hash" wird von "blabla" getrennt
    for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=", 2);
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n)) parms[n] = [];
        parms[n].push(nv.length === 2 ? v : null);
    }
    theUrl = pairs;
    payUnityId = parms.id;
    console.log(
        parms
    );
    return theUrl;
}
var jumbotronP = document.getElementById('jumbotronP');
var payUnitQueryResponseCode = "";
var jumbotronText = "";
var payUnitQueryResponseText404 = "";
var paymentInfo;
var db;


function payUnitQuery() {
    var paystring =
        "authentication.userId=8a8294176059beb00160692d1852120e" +
        "&authentication.password=6tpwBHCk3e" +
        "&authentication.entityId=8a8294176059beb00160692d4b471213";

    fetch('https://test.oppwa.com/v1/checkouts/' + payUnityId + '/payment?' + paystring, {}).then(function (response) {
        return response.json();
    }).then(function (data) {
        paymentInfo = data;
        console.log(data);
        switch (data.result.code) {
            case "200.300.404":
                payUnitQueryResponseCode = "200.300.404";
                payUnitQueryResponseText404 = data.result.description;
                $('.jumbotron').removeClass("jumboWait").addClass("jumboError");
                $('body').removeClass("bodyWait").addClass("bodyError");
                jumbotronP.textContent = "Diese Transaktion endete leider mit einem Fehler! statuscode: " + payUnitQueryResponseText404 + " " + payUnitQueryResponseCode;
                break;
            case "000.100.110":

                var successAnker = document.getElementById('cardSuccess');
                var briefKopf = document.createElement('p');
                $(briefKopf).attr({
                    "id": "briefKopf"
                });
                briefKopf.textContent = "OTAS Computer Software GmbH" + br + "Haymogasse 57" + br + "1230 Wien";
                successAnker.appendChild(briefKopf);
                var hr1 = document.createElement('hr');
                successAnker.appendChild(hr1);
                var zahlungsBest = document.createElement('div');
                $(zahlungsBest).attr({
                    "id": "zahlungsBest"
                });
                zahlungsBest.textContent = "Der Betrag von  : " + data.amount + "€" + br + "wurde an die" + br + data.paymentBrand + " Karte" + br + "mit Endziffern : *******" + data.card.last4Digits + " " + br + "Ablaufjahr : " + data.card.expiryYear + " " + br + "AblaufMonat : " + data.card.expiryMonth + "" + br + " des Besitzers : " + data.card.holder + br + " erfolgreich angewiesen." + br + "Für Supportfragen speichern" + br + " Sie bitte diese" + br + "Transaktionnummer  : " + br + data.resultDetails.reconciliationId + br + "oder drücken Sie auf den" + br + "Kameraknopf um einen 'Screenshot'" + br + "zu machen";
                successAnker.appendChild(zahlungsBest);
                var hr2 = document.createElement('hr');
                successAnker.appendChild(hr2);

                var zahlungsFooter = document.createElement('div');
                $(zahlungsFooter).attr({
                    "id": "zahlungsFooter"
                });
                successAnker.appendChild(zahlungsFooter);
                $(screenShotButton).show();
                $(cardSuccess).show();
                var shotDiv = document.getElementById("screenshot");
                $(shotDiv).show();
                payUnitQueryResponseCode = "000.100.110";
                $('.jumbotron').removeClass("jumboWait").addClass("jumboSuccess");
                $('body').removeClass("bodyWait").addClass("bodySuccess");
                jumbotronP.textContent = "Vielen Dank für Ihren Einkauf!";
                break;
            default:
                $('.jumbotron').removeClass("jumboWait").addClass("jumboDefault");
                $('body').removeClass("bodyWait").addClass("bodyDefault");
                jumbotronP.textContent = "Zahlung wurde versucht aber ergab statuscode: " + payUnityId + " " + payUnitQueryResponseCode;
                jumbotronP.style.backgroundColor = '#e180f5';
                break;
        }
        paymentInfoTest(paymentInfo);
    }).catch(function (err) {
        console.log(err);
    });
}

function paymentInfoTest(pnfo) {
    console.log(pnfo);
}

function ansichtElementeErstellen() {
    jumbotronP.textContent(jumbotronText);
}

document.addEventListener("DOMContentLoaded", function (event) {
    payUnitQuery();
});

function toApp() {
    location.href = Hosting;
}

function makeScreenshot() {
    photoFlash();
    html2canvas(document.querySelector('#screenshot'), {
        scale: 2
    }).then(function (canvas) {
        var isCanvas = document.querySelectorAll('canvas');
        if (isCanvas.length != 0) {
            isCanvas[0].parentNode.removeChild(isCanvas[0]);
        }
        document.body.appendChild(canvas);
        console.log(isCanvas);
    });
}

function photoFlash() {
    $("body").addClass("flashOn");
    setTimeout(function(){$("body").removeClass("flashOn");},1000);
    $('html, body').animate({scrollTop:$(document).height()}, 'slow');
}