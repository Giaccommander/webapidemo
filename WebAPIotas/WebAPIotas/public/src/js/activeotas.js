//Support für ältere Browser ohne fetch und promise 
if (!window.Promise) {
    window.Promise = Promise;
}

var localHosting = "http://localhost";
var iisHosting = "http://tankstelle.otas.at";
var iisSHosting = "https://tankstelle.otas.at";
var Hosting = localHosting; //Hier bitte passende Addresse einfügen

// -------------------------CSS und passendes Iframe werden eingefügt-----------------------
var cssName = "/src/css/homeSPA.css";
var urlQuery = window.location.href;
var newCSS = new Promise(
    function (resolve, reject) {
        var url = urlQuery;
        var queryStart = url.indexOf("?") + 1,
            queryEnd = url.indexOf("#") + 1 || url.length + 1,
            query = url.slice(queryStart, queryEnd - 1),
            pairs = query.replace(/%2F/g, "\+");
        pairs = query.replace(/\+/g, " ").split("&");
        parms = {};
        var n, v, nv;

        if (query === url || query === "") return;
        //für url´s die key und value senden z.b. http://blabla.at/user/activate?hash=blabla "hash" wird von "blabla" getrennt
        for (i = 0; i < pairs.length; i++) {
            nv = pairs[i].split("=", 2);
            n = decodeURIComponent(nv[0]);
            v = decodeURIComponent(nv[1]);

            if (!parms.hasOwnProperty(n)) parms[n] = [];
            parms[n].push(nv.length === 2 ? v : null);
        }
        // console.log(pairs);
        // console.log(query);
        // console.log(parms.css);
        if (typeof parms != undefined && parms != null) {
            resolve(parms);
        } else {
            reject("Es wurde kein css in der URL angegeben? (z.b: http://tankstelle.otas.at/indes.html?css=leikermoser) Dann können Sie diesen fehler ignorieren.");
        }
    });

// in der url kann ein css=[kundenname] als query mitgeschickt werden 

if (urlQuery !== "undefined") {
    newCSS.then(function (parms) {
        if (parms.css === undefined) {
            newCSS.catch();
        } else {
            if (parms.css[0] == "leikermoser" || parms.css[0] == "turmoel" || parms.css[0] == "gutmann") {
                switch (parms.css[0]) {

                    case "leikermoser":
                        cssName = "/src/css/leikermoser.css";
                        cssSelector(cssName);
                        break;
                    case "turmoel":
                        cssName = "/src/css/turmoel.css";
                        cssSelector(cssName);
                        break;
                    case "gutmann":
                        cssName = "/src/css/gutmann.css";
                        cssSelector(cssName);

                }
            } else {
                cssName = "";
                cssSelector(cssName);
                return cssName;
            }
        }
    });

    newCSS.catch(function () {
        cssName = "";
        cssSelector(cssName);
        console.log("reject");
    });
}

function cssSelector(cssName) {
    document.onreadystatechange = function () {
        if (document.readyState == "complete") {
            var cssCheck = document.getElementsByTagName("link");
            if (cssName == "undefined" || cssName == "") {
                return;
            } else {
                for (var k = cssCheck.length; k >= 0; k--) {
                    if ($(cssCheck[k]).attr("href") == "/src/css/homeSPA.css") {
                        removejscssfile("/src/css/homeSPA.css", "css");
                    }
                }
            }
        }
    };

    function removejscssfile(filename, filetype) {
        var targetelement = (filetype == "js") ? "script" : (filetype == "css") ? "link" : "none"; //determine element type to create nodelist from
        var targetattr = (filetype == "js") ? "src" : (filetype == "css") ? "href" : "none"; //determine corresponding attribute to test for
        var allsuspects = document.getElementsByTagName(targetelement);
        for (var i = allsuspects.length; i >= 0; i--) { //search backwards within nodelist for matching elements to remove
            if (allsuspects[i] && allsuspects[i].getAttribute(targetattr) != null && allsuspects[i].getAttribute(targetattr).indexOf(filename) != -1)
                allsuspects[i].parentNode.removeChild(allsuspects[i]); //remove element by calling parentNode.removeChild()
        }
    }


    var headAnker = document.getElementsByTagName("head");
    var fileref = document.createElement("link");
    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", cssName);
    if (typeof fileref != "undefined")
        headAnker[0].appendChild(fileref);
}