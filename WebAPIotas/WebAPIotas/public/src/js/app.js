if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js').then(function () {});
    console.log("ServiceWorker in app.js installed");
}

//Support für ältere Browser ohne fetch und promise 
if (!window.Promise) {
    window.Promise = Promise;
}

var localHosting = "https://localhost:44302/public";
var iisHosting = "http://tankstelle.otas.at";
var iisSHosting = "https://tankstelle.otas.at";
var Hosting = localHosting; //Hier bitte passende Addresse einfügen

// -------------------------CSS und passendes Iframe werden eingefügt-----------------------
var cssName = "src/css/homeSPA.css";
var urlQuery = window.location.href;
var newCSS = new Promise(
    function (resolve, reject) {
        var url = urlQuery;
        var queryStart = url.indexOf("?") + 1,
            queryEnd = url.indexOf("#") + 1 || url.length + 1,
            query = url.slice(queryStart, queryEnd - 1),
            pairs = query.replace(/%2F/g, "\+");
        pairs = query.replace(/\+/g, " ").split("&");
        parms = {};
        var n, v, nv;

        if (query === url || query === "") return;
        //für url´s die key und value senden z.b. http://blabla.at/user/activate?hash=blabla "hash" wird von "blabla" getrennt
        for (i = 0; i < pairs.length; i++) {
            nv = pairs[i].split("=", 2);
            n = decodeURIComponent(nv[0]);
            v = decodeURIComponent(nv[1]);

            if (!parms.hasOwnProperty(n)) parms[n] = [];
            parms[n].push(nv.length === 2 ? v : null);
        }
        // console.log(pairs);
        // console.log(query);
        // console.log(parms.css);
        if (typeof parms != undefined && parms != null) {
            resolve(parms);
        } else {
            reject("Es wurde kein css in der URL angegeben? (z.b: http://tankstelle.otas.at/indes.html?css=leikermoser) Dann können Sie diesen fehler ignorieren.");
        }
    });

// in der url kann ein css=[kundenname] als query mitgeschickt werden 

if (urlQuery !== "undefined") {
    newCSS.then(function (parms) {
        if (parms.css === undefined) {
            newCSS.catch();
        } else {
            if (parms.css[0] == "leikermoser" || parms.css[0] == "turmoel" || parms.css[0] == "gutmann") {
                switch (parms.css[0]) {

                    case "leikermoser":
                        cssName = "src/css/leikermoser.css";
                        cssSelector(cssName);
                        break;
                    case "turmoel":
                        cssName = "src/css/turmoel.css";
                        cssSelector(cssName);
                        break;
                    case "gutmann":
                        cssName = "src/css/gutmann.css";
                        cssSelector(cssName);

                }
            } else {
                cssName = "";
                cssSelector(cssName);
                return cssName;
            }
        }
    });

    newCSS.catch(function () {
        cssName = "";
        cssSelector(cssName);
        console.log("reject");
    });
}
// var stationenIframe = document.getElementById('stationenIframe');
// if (urlQuery == "http://localhost:8080/#stationenSection" || urlQuery == "http://localhost:8080/#loginSection" || urlQuery == "http://localhost:8080/#homeSection") {
//     $(stationenIframe).attr('src', 'https://www.gutmann.cc/index.php?id=74');
//      https:www.lm-energy.at/tankstellen/tankstellen-netz/
// }

function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1,
        queryEnd = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/%2F/g, "\+");
    pairs = query.replace(/\+/g, " ").split("&");
    parms = {};
    var n, v, nv;

    if (query === url || query === "") return;
    //für url´s die key und value senden z.b. http://blabla.at/user/activate?hash=blabla "hash" wird von "blabla" getrennt
    for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=", 2);
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n)) parms[n] = [];
        parms[n].push(nv.length === 2 ? v : null);
    }
    // console.log(pairs);
    // console.log(query);
    // console.log(parms);
    return;
}

// homeSPA.css wird entfernt und durch [cssName].css ersetzt
function cssSelector(cssName) {
    document.onreadystatechange = function () {
        if (document.readyState == "complete") {
            var cssCheck = document.getElementsByTagName("link");
            if (cssName == "undefined" || cssName == "") {
                return;
            } else {
                for (var k = cssCheck.length; k >= 0; k--) {
                    if ($(cssCheck[k]).attr("href") == "src/css/homeSPA.css") {
                        removejscssfile("src/css/homeSPA.css", "css");
                    }
                }
            }
        }
    };

    function removejscssfile(filename, filetype) {
        var targetelement = (filetype == "js") ? "script" : (filetype == "css") ? "link" : "none"; //determine element type to create nodelist from
        var targetattr = (filetype == "js") ? "src" : (filetype == "css") ? "href" : "none"; //determine corresponding attribute to test for
        var allsuspects = document.getElementsByTagName(targetelement);
        for (var i = allsuspects.length; i >= 0; i--) { //search backwards within nodelist for matching elements to remove
            if (allsuspects[i] && allsuspects[i].getAttribute(targetattr) != null && allsuspects[i].getAttribute(targetattr).indexOf(filename) != -1)
                allsuspects[i].parentNode.removeChild(allsuspects[i]); //remove element by calling parentNode.removeChild()
        }
    }


    var headAnker = document.getElementsByTagName("head");
    var fileref = document.createElement("link");
    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", cssName);
    if (typeof fileref != "undefined")
        headAnker[0].appendChild(fileref);
}
// -------------------------CSS und passendes Iframe werden eingefügt-----------------------

window.addEventListener('beforeinstallprompt', function (event) {
    console.log('beforeinstallprompt fired');
    event.preventDefault();
    deferredPrompt = event;
    return false;
});
var sessionValue = sessionStorage.getItem('hash');
var sessionKey = sessionStorage.getItem('email');
var storageValue = localStorage.getItem('hash');
var storagKey = localStorage.getItem('email');
document.addEventListener("DOMContentLoaded", function () {
    try {
        sessionCheck();
    } catch (error) {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-full-width",
            "preventDuplicates": true,
            "showDuration": "200",
            "hideDuration": "500",
            "timeOut": 0,
            "extendedTimeOut": 0,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        };
        toastr.error("Session Speicher leer");
    }
});

// From your client pages:
if ((navigator.platform.indexOf("iPhone") != -1) ||
    (navigator.platform.indexOf("iPod") != -1) ||
    (navigator.platform.indexOf("iPad") != -1)) {
    console.log("IOS");
} else {
    var channel = new BroadcastChannel('sw-messages');
    channel.addEventListener('message', function (event) {
        var messa = event.data;
        document.getElementById("swVersion").innerHTML = messa.title;
    });
}


// navbar collapser bei Berührung wird die Navbar wieder geschlossen
$('.navbar-collapse a').click(function () {
    $(".navbar-collapse").collapse('hide');
});

$(document).click(function (e) {
    if (!$(e.target).is('.navbar-collapse a')) {
        $('.navbar-collapse').collapse('hide');
    }
});

//Navbar wird auf loginSection und registerSection nicht gezeigt. Auf allen anderen Sectionen/Pages sollte die Navbar sichtbar sein.
function showPageNonav(pageId) {
    $(".page").hide();
    $(pageId).show();
    $(".navbar").hide();
    $("#homeTabButton").hide();
    location.hash = pageId;

}

function showPage(pageId) {
    $(".page").hide();
    $(pageId).show();
    $(".navbar").show();
    $("#homeTabButton").show();
    location.hash = pageId;

}


location.hash = "";
location.hash = "#loginSection";

$(window).on("hashchange", function () {
    var sessionValue = sessionStorage.getItem("hash");

    if (sessionValue == null || sessionValue == "") {
        if (location.hash == "#registerSection" || location.hash == "#iForgotPassword") {
            hashchange();
            return;
        } else {
            location.hash = "#loginSection";
        }
    }
    hashchange();
});

function hashchange(event) {
    if (location.hash == "#registerSection" || location.hash == "#loginSection" || location.hash == "#iForgotPassword") {
        showPageNonav(location.hash);
    } else {
        showPage(location.hash);
    }

    $(".nav-link", "btn").each(function () {
        if ($(this).attr("href") || $(this).attr("onclick") === location.hash) {
            $(this).addClass("selected");
        } else {
            $(this).removeClass("selected");
        }
    });
    if (location.hash == '#gutscheinSection') {
        showSyncButton();
    } else {
        hideSyncButton();
    }
    if (location.hash != '#meineDatenSection') {
        bearbeitenToggle.checked = false;
        disableMyDataFields();
        meinPasswortChangeAreaOff();
    }
}

// ---------------------------------------Navbar------------------------------------------
var syncButton = document.getElementById('syncButton');

function showSyncButton() {
    $('#syncButton').show();
}

function hideSyncButton() {
    $('#syncButton').hide();
}
// --------------------------------------------------Loading Funktion Ein/AUS
var spinnerShow = document.getElementById('spinnerShow');
var spinnerBlurr = document.getElementsByClassName('mobileShow1');
var tabHide = document.getElementsByClassName('nav-pills');

function loadingSpinnerEIN() {
    $(spinnerBlurr).addClass('blurr');
    $(spinnerBlurr).hide();
    $(tabHide).hide();
    $(spinnerShow).removeClass('d-none');
    $('#abortButton').show();
    spinnerCheckBool = true;
}

function loadingSpinnerAUS() {
    $(spinnerBlurr).removeClass('blurr');
    $(spinnerBlurr).show();
    $(tabHide).show();
    $(spinnerShow).addClass('d-none');
    $('#abortButton').hide();
    spinnerCheckBool = false;
}


// -------------------------Registrierung-----------------------------------------------------



// regEx für passwort 1groß,1nummer,1zeichen,mindestens 8, maximal 20
function CheckPassword() {
    var registerPasswort = document.getElementById('registerPasswort').value;
    var registerPasswort2 = document.getElementById('registerPasswort2').value;
    var regEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,20}$/;
    if (registerPasswort.match(regEx)) {

        if (registerPasswort != registerPasswort2) {
            toastr.warning("Passwörter sind nicht identisch");
            return false;
        } else {
            SendRegPostForm();
            return;
        }
    } else {
        toastr.info('Mindestens 7 Zeichen,<br/> 1 Großbuchstabe,<br/> 1 Sonderzeichen,<br/> 1 Zahl');
        return false;
    }
}


function SendRegPostForm() {
    loadingSpinnerEIN();
    var firstname = document.getElementById("registerVorname").value;
    var lastname = document.getElementById("registerNachname").value;
    var email = document.getElementById("registerEmail").value;
    var password = document.getElementById('registerPasswort').value;
    var encryptedpassword = null;
    var deliverCustomerId = null;
    var salt = null;
    var IsActivated = null;

    var tempUser = {
        "deliverCustomerId": deliverCustomerId,
        "email": email,
        "firstname": firstname,
        "lastname": lastname,
        "password": password,
        "encryptedpassword": encryptedpassword,
        "salt": salt,
        "IsActivated": IsActivated
    };
    fetch(Hosting + ':8500/user/register', {
            method: 'POST',
            signal: signal,
            headers: {
                'Content-Type': 'application/json'

            },
            body: JSON.stringify(tempUser)
        }).then(function (response) {
            // console.log(response);
            return response.json();
        }).then(function (data) {
            if (data.successful == true) {
                toastr.success("E-Mail versendet. Bitte die Email bestätigen");
                loadingSpinnerAUS();
                return showPageNonav('#loginSection');
            } else {
                loadingSpinnerAUS();
                toastr.warning("Kein Zugriff auf Datenbank möglich.");
                console.log(data + " " + "Error accured");
            }
        })
        .catch(function (err) {
            if (err.message == "The user aborted a request.") {
                toastr.warning("Vorgang auf wunsch abgebrochen!");
            } else {
                toastr.error("Error :" + err);
            }
            controller = new AbortController();
            signal = controller.signal; //resettet das signal
            loadingSpinnerAUS();
        });

}

// --------------Login----------------------------------------------------------------------
var voucherReady = false;

function sessionCheck() {
    loadingSpinnerEIN();
    if (sessionValue == null || sessionValue == "") {
        if (storageValue == null || storageValue == "") {
            loadingSpinnerAUS();
            return;
        } else {
            voucherReady = true;
            sessionValue = storageValue;
            sessionKey = storagKey;
            toastr.options = {
                "positionClass": "toast-bottom-full-width",
                "timeOut": "2000"
            };
            toastr.options.hideMethod = 'slideUp';
            toastr.success("welcome");
            getUserData(storageValue);
            loadingSpinnerAUS();
        }
        return;
    } else {
        voucherReady = true;
        loadingSpinnerAUS();
        toastr.options = {
            "positionClass": "toast-bottom-full-width",
            "timeOut": "2000"
        };
        toastr.options.hideMethod = 'slideUp';
        toastr.success("welcome");
        getUserData(sessionValue);
    }
    loadingSpinnerAUS();
    return;
}


function sendLoginForm() {
    loadingSpinnerEIN();
    var loginEmail = document.getElementById('loginName').value;
    var loginPassword = document.getElementById('loginPasswort').value;
    var stayLogged = document.getElementById('stayLogged');
    var loginTempUser = {
        "email": loginEmail,
        "password": loginPassword
    };
    fetch(Hosting + ':8500/app/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(loginTempUser)
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            var loginData = data;
            if (loginData.successful == true) {
                voucherReady = true;
                if (stayLogged.checked == true) {
                    localStorage.setItem('email', loginData.email);
                    localStorage.setItem('hash', loginData.sessionid);
                    sessionStorage.setItem('email', loginData.email);
                    sessionStorage.setItem('hash', loginData.sessionid);
                    toastr.success("Sie sind dauerhaft eingeloggt");
                } else {
                    sessionStorage.setItem('email', loginData.email);
                    sessionStorage.setItem('hash', loginData.sessionid);
                    toastr.success("Sie sind eingeloggt");
                }
                // console.log(sessionStorage.getItem('hash') + sessionStorage.getItem('email'));

                getUserData(loginData.sessionid);
                loadingSpinnerAUS();
            } else {
                loadingSpinnerAUS();
                toastr.warning("Diese Mail-Passwort Kombination wurde nicht gefunden:\n" + data.message);
                return data.message;
            }
        })
        .catch(function (err) {
            loadingSpinnerAUS();
            console.log("Hier ist ein fehler: " + err);
        });
}


// --------------------------------------- HomeSection ------------------------------------------

function getUserData(sessionid) {

    if (voucherReady) {

        fetch(Hosting + ':8500/app/getuserdata', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'

            },
            body: JSON.stringify(sessionid)
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            document.getElementById("karteName").innerHTML = data.firstname + " " + data.lastname;
            document.getElementById("karteNummer").innerHTML = "Nr.: " + data.cardNo;
            // if (data.plate == "") {
            //     data.plate = "NoPlate";
            // }
            document.getElementById("plate").innerHTML = data.plate;
            var qrAnker = document.querySelector('#qrAnker');
            if (qrAnker.firstChild != null) {
                qrAnker.removeChild(qrAnker.firstChild);
            }
            var qrCodeHome = document.createElement('div');
            $(qrCodeHome).attr({
                "id": "qrcodeHome",
                "style": "width:100px"
            });
            qrAnker.appendChild(qrCodeHome);
            jQuery('#qrcodeHome').qrcode({
                text: data.track2,
                ecLevel: 'H',
                size: 100
            });
            // console.log(data);
        }).catch(function (err) {
            console.log("Hier ist ein fehler: " + err);
        });
    }
    getStationList(sessionid);
    showPage("homeSection");
}



// --------------------------------------------------- GutscheinSection -------------------------------

var emptyGutscheineLeeren = document.getElementById('emptyVoucherShow');
var emptyRedeemedGutscheineLeeren = document.getElementById('emptyRedeemedVoucherShow');
var VoucherHideTabAktuell = document.getElementById("voucherHideTabAktuell");
var VoucherHideTabAlt = document.getElementById("voucherHideTabAlt");
$('#voucherHideTabAktuell').addClass("clickedTab");
$("#voucherHideTabAktuell").click(function () {
    $("#accordionListeALT").hide();
    $("#emptyRedeemedVoucherShow").hide();
    $("#voucherHideTabAlt").removeClass("clickedTab");
    $('#accordionListe').show();
    $('#emptyVoucherShow').show();
    $('#voucherHideTabAktuell').addClass("clickedTab");

});

$("#voucherHideTabAlt").click(function () {
    $("#accordionListe").hide();
    $("#emptyVoucherShow").hide();
    $("#voucherHideTabAktuell").removeClass("clickedTab");
    $('#accordionListeALT').show();
    $('#emptyRedeemedVoucherShow').show();
    $('#voucherHideTabAlt').addClass("clickedTab");
});

function reloadIcon() {
    var spiner = document.getElementById('gutscheinSyncButton');
    $(spiner).addClass('refreshing');
    gutscheinNavigator();
    setTimeout(function () {
        $(spiner).removeClass('refreshing');
    }, 2000);
}




function gutscheinNavigator() {
    loadingSpinnerEIN();
    var gutscheineLeeren = document.getElementById('accordionListe');
    var redeemedgutscheineLeeren = document.getElementById('accordionListeALT');

    if (emptyRedeemedGutscheineLeeren.firstChild != null) {
        while (emptyRedeemedGutscheineLeeren.firstChild) {
            emptyRedeemedGutscheineLeeren.removeChild(emptyRedeemedGutscheineLeeren.firstChild);
        }
    } else {

        while (redeemedgutscheineLeeren.firstChild) {
            redeemedgutscheineLeeren.removeChild(redeemedgutscheineLeeren.firstChild);
        }
    }

    if (emptyGutscheineLeeren.firstChild != null) {
        while (emptyGutscheineLeeren.firstChild) {
            emptyGutscheineLeeren.removeChild(emptyGutscheineLeeren.firstChild);
        }
    } else {

        while (gutscheineLeeren.firstChild) {
            gutscheineLeeren.removeChild(gutscheineLeeren.firstChild);
        }
    }
    var sessionid = sessionStorage.getItem('hash');
    getVocher(sessionid);
}



var controller = new AbortController();
var signal = controller.signal;

function abortFechting() {
    controller.abort();
}

function getVocher(sessionid) {
    if (voucherReady) {
        fetch(Hosting + ':8500/app/voucherlist', {
            method: 'POST',
            signal: signal,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(sessionid)
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            // console.log(data);
            voucherLoop(data);
            loadingSpinnerAUS();
        }).catch(function (err) {
            if (err.message == "The user aborted a request.") {
                toastr.warning("Vorgang auf wunsch abgebrochen!");
            } else {
                toastr.error("Hier ist ein getVoucher-fehler: " + err);
            }
            controller = new AbortController();
            signal = controller.signal; //resettet das signal            
            loadingSpinnerAUS();
            return controller;
        });
        return controller;
    }

}



function voucherLoop(vouchers) {
    var vouchercnt = 0;
    var redeemedVoucher = [];
    gutscheine = vouchers;
    if (gutscheine.VoucherCollection.length == 0 || gutscheine.VoucherCollection.length == null) {
        emptyRedeemedVoucherShow();
        emptyVoucherShow();

    } else {
        for (var i = 0; i < gutscheine.VoucherCollection.length; i++) {
            if (gutscheine.VoucherCollection[i].DateRedeemed != null) {
                redeemedVoucher.push(gutscheine.VoucherCollection[i]);
                continue;
            }
            var cardQrcode = gutscheine.VoucherCollection[i].VoucherId;
            // var cardStartDate = gutscheine.VoucherCollection[i].startDate;
            var cardExpireDate = gutscheine.VoucherCollection[i].VoucherExpiry;
            // var cardJSBarcode = gutscheine.VoucherCollection[i].VoucherId;
            var cardText = gutscheine.VoucherCollection[i].ConsecutiveVoucherNo;
            var cardDescription = gutscheine.VoucherCollection[i].ItemDescription;
            var cardValue;
            if (gutscheine.VoucherCollection[i].VoucherValue == null) {
                cardValue = " ";
            } else {
                cardValue = gutscheine.VoucherCollection[i].VoucherValue;
            }
            var bg = "#8a8a8a";
            var accordionselector = true;
            var datumText = "Gültig bis: ";
            // console.log(cardQrcode+cardStartDate+cardExpireDate+cardJSBarcode+cardText+cardText+cardValue);
            createCard(cardQrcode, cardExpireDate, cardText, cardDescription, cardValue, bg, accordionselector, datumText);
            vouchercnt++;

        }
        if (vouchercnt == 0) {

        } //NUR wenn keine Aktuellen Gutscheine vorhanden sind sollte das angezeigt werden
        if (redeemedVoucher.length != 0) {
            i = 0;
            for (i = 0; i < redeemedVoucher.length; i++) {
                var deemedcardQrcode = redeemedVoucher[i].VoucherId;
                // var deemedcardStartDate = redeemedVoucher[i].startDate;
                var deemedcardExpireDate = redeemedVoucher[i].DateRedeemed;
                //ACHTUNG HIER DateRedeemed statt VoucherExpiry. Bei den alten Gutscheinen wird das ExpireDatum angezeigt
                // var deemedcardJSBarcode = redeemedVoucher[i].VoucherId;
                var deemedcardText = redeemedVoucher[i].ConsecutiveVoucherNo;
                var deemedcardDescription = redeemedVoucher[i].ItemDescription;
                var deemedcardValue;
                if (redeemedVoucher[i].VoucherValue == null) {
                    deemedcardValue = " ";
                } else {
                    deemedcardValue = redeemedVoucher[i].VoucherValue;
                }
                var deemedbg = "#f2f2f2";
                var deemedaccordionSelector = false;
                var deemeddatumText = "Eingelöst am: ";
                // console.log(cardQrcode+cardStartDate+cardExpireDate+cardJSBarcode+cardText+cardText+cardValue);
                createCard(deemedcardQrcode, deemedcardExpireDate, deemedcardText, deemedcardDescription, deemedcardValue, deemedbg, deemedaccordionSelector, deemeddatumText);
            }
        } else {
            emptyRedeemedVoucherShow();
        }
    }
    if (vouchercnt === 0) {
        emptyVoucherShow();
    }
}

function emptyVoucherShow() {
    var emptyVocherAnker = document.getElementById('emptyVoucherShow');
    var emptyVoucherDiv1 = document.createElement('div'); //div1
    emptyVoucherDiv1.className = 'text-centered';
    var emptyVoucherDiv2 = document.createElement('div'); //div2
    emptyVoucherDiv2.className = 'text-centered';
    emptyVoucherDiv2.textContent = 'Sie haben zur Zeit leider keine Gutscheine... aber hoffentlich bald';
    $(emptyVoucherDiv2).attr({
        "id": "emptyVoucherId"
    });
    emptyVoucherDiv1.appendChild(emptyVoucherDiv2);
    emptyVocherAnker.appendChild(emptyVoucherDiv1);
    vouchercnt++;
}

function emptyRedeemedVoucherShow() {
    // var trennLinie = document.getElementById('gutscheineTrennlinie');
    // trennLinie.style.display = 'none';
    var emptyRedeemedVocherAnker = document.getElementById('emptyRedeemedVoucherShow');
    emptyRedeemedVocherAnker.style.display = 'none';
    var emptyRedeemedVoucherDiv1 = document.createElement('div'); //div1
    emptyRedeemedVoucherDiv1.className = 'text-centered';
    var emptyRedeemedVoucherDiv2 = document.createElement('div'); //div2
    emptyRedeemedVoucherDiv2.className = 'text-centered';
    emptyRedeemedVoucherDiv2.textContent = 'Sie haben noch keine Gutscheine verbraucht.';
    $(emptyRedeemedVoucherDiv2).attr({
        "id": "emptyRedeemedVoucherId"
    });
    emptyRedeemedVoucherDiv1.appendChild(emptyRedeemedVoucherDiv2);
    emptyRedeemedVocherAnker.appendChild(emptyRedeemedVoucherDiv1);
}

// ---------------------StationSection--------------------


function getStationList(sessionid) {
    fetch(Hosting + ':8500/app/stationlist', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(sessionid)
    }).then(function (response) {
        return response.json();
    }).then(function (data) {
        // console.log(data);
        stationenLoop(data);
    }).catch(function (err) {
        console.log("Hier ist ein fehler: " + err);
    });
}

function stationenLoop(stationen) {

    function ResetActiveShow() {
        if (document.querySelector('#stationenAnker div.tab-content').firstChild !== null) {
            document.querySelector('#stationenAnker div.tab-content').classList.remove('show');
            document.querySelector('#stationenAnker div.tab-content').classList.remove('active');
        }
        if (document.querySelector('#stationenAnker2 div.list-group').firstChild !== null) {
            document.querySelector('#stationenAnker2 div.list-group').classList.remove('active');
        }
    }
    ResetActiveShow();
    var stationsArray = stationen;
    var stationsCounter = 0;
    var activeClassName = " ";
    var showactiveClass = " ";
    if (stationsArray.StationCollection.length == 0 || stationsArray.StationCollection.length == null) {
        console.log("keine Stationen verfügbar?!");
    } else {

        try {
            for (var i = 0; i < stationsArray.StationCollection.length; i++) {

                var stationAddress = stationsArray.StationCollection[i].Address;
                var stationDescription = stationsArray.StationCollection[i].Description;
                var stationsCity = stationsArray.StationCollection[i].City;
                var stationsLatLong = stationsArray.StationCollection[i].LatLong;
                if (stationsArray.StationCollection[i].LatLong == null) {
                    continue;
                }
                var splitter = stationsLatLong.split(',');
                var stationsLat = splitter[0];
                var stationLong = splitter[1];
                var stationMail = stationsArray.StationCollection[i].Mail;
                var stationBrandName = stationsArray.StationCollection[i].StationBrandName;
                var stationNo = stationsArray.StationCollection[i].StationNo;
                var stationTel = stationsArray.StationCollection[i].Telephone;
                var stationZip = stationsArray.StationCollection[i].ZipCode;
                if (stationsCounter == 0) {
                    activeClassName = ' active';
                    showactiveClass = 'show active';
                } else {
                    activeClassName = " ";
                    showactiveClass = " ";
                }
                tempAddress = stationAddress + " " + stationZip + " " + stationsCity;
                createStationen(tempAddress, stationsLat, stationLong, stationsLatLong, stationBrandName, stationDescription, activeClassName, showactiveClass, stationTel);
                stationsCounter++;
            }
        } catch (err) {

            console.log(err);
        }
    }

}


// function myNavFunc(lat, long) {
//     // If it's an iPhone..
//     if ((navigator.platform.indexOf("iPhone") != -1) ||
//         (navigator.platform.indexOf("iPod") != -1) ||
//         (navigator.platform.indexOf("iPad") != -1))
//         window.open("maps://maps.google.com/maps?daddr=" + lat + "," + long + "&amp;ll=");
//     else
//         window.open("https://www.google.com/maps/search/?api=1&query=" + lat + "," + long);
// }
// ------------------------------------------MeineDaten Section--------------------------------------

// Meine DummyDaten:
var myUser = {
    meineEmail: 'bamhwererst@gmx.at',
    kundenNummer: "1001588",
    vorName: "Giaccomo",
    nachName: "Sildfhl",
    strasse: "Eliterdgsgslatz 6/4/10",
    zip: "1170",
    ort: "Wien",
    myPasswort: "Pass1123!",
    myPasswort2: "Pass1123!",
    tel: "06509801544",
    rabatt: "2 cent pro Liter Diesel, 5% auf Getränke"
};

function getUserAllData(sessionid) {
    fetch(Hosting + ':8500/app/getuseralldata', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(sessionid)
    }).then(function (response) {
        return response.json();
    }).then(function (data) {
        // console.log(data);
        myUser.meineEmail = data.email;
        myUser.kundenNummer = data.kundenNummer;
        myUser.vorName = data.firstname;
        myUser.nachName = data.lastname;
        myUser.strasse = data.strasse;
        myUser.zip = data.ZipCode;
        myUser.ort = data.City;
        myUser.myPasswort = data.password;
        myUser.myPasswort2 = data.password;
        myUser.tel = data.telNr;
    }).catch(function (err) {
        console.log("Hier ist ein fehler: " + err);
    });

}

// kleines x zum löschen des input Inhaltes findet das Inputfeld und löscht den Inhalt
$('.fa-times-circle-o').click(function () {
    var sibling = $(this).siblings();
    sibling[0].value = "";
    sibling = [];
});

// Alle Felder, die bearbeitbar werden sollen
var myDataEmail = document.getElementById('myDataEmail');
myDataEmail.placeholder = myUser.meineEmail;
var myDataVorname = document.getElementById('myDataVorname');
myDataVorname.placeholder = myUser.vorName;
var myDataNachname = document.getElementById('myDataNachname');
myDataNachname.placeholder = myUser.nachName;
var myDataStrasse = document.getElementById('myDataStrasse');
myDataStrasse.placeholder = myUser.strasse;
var myDataZip = document.getElementById('myDataZip');
myDataZip.placeholder = myUser.zip;
var myDataOrt = document.getElementById('myDataOrt');
myDataOrt.placeholder = myUser.ort;
var myDataTel = document.getElementById('myDataTel');
myDataTel.placeholder = myUser.tel;
var myDataRabatt = document.getElementById('myDataRabatt');
myDataRabatt.placeholder = myUser.rabatt;
var myDataButton = document.getElementById('myDataButton');
var myDataInputClearIcons = document.getElementsByClassName('fa-times-circle-o');
$(myDataButton).click(function () {
    if (!myDataButton.getAttribute('disabled')) {
        // console.log('button was not disabled and been clicked');
    } else {
        console.log('this shouldn´t been seen');
    }

});

var formFieldsArray = [
    myDataEmail,
    myDataVorname,
    myDataNachname,
    myDataStrasse,
    myDataZip,
    myDataOrt,
    myDataTel
];


function enableMyDataFields() {
    formFieldsArray.forEach(function (i) {
        i.removeAttribute('disabled', true);
    });
    myDataButton.removeAttribute('disabled', true);
    $(myDataInputClearIcons).show();

}

function disableMyDataFields() {

    formFieldsArray.forEach(function (i) {
        i.setAttribute('disabled', false);
    });
    myDataButton.setAttribute('disabled', false);
    $(myDataInputClearIcons).hide();
    var inputBool = false;
    formFieldsArray.forEach(function (i) {
        if (i.value != "" || i.value.length > 0) {
            console.log("in diesem Feld wurde etwas verändert..." + i);
            inputBool = true;
        } else {
            // console.log("da steht NIX drinn..." + i);
        }
    });

    if (inputBool == true) {
        saveUpdatesOnMyData();
    }

}

function saveUpdatesOnMyData() {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-full-width",
        "preventDuplicates": false,
        "onclick": false,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": 0,
        "extendedTimeOut": 0,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": true
    };

    toastr.info("<br />Daten Speichern?<br /><br /><button type='button' onClick='resetData()' class='btn' id='doNotsaveButton'>Nein</button><button type='button' onClick='sendNewData()' class='btn' id='saveButton'> JA </button>");
}

function sendNewData() {
    console.log("daten gespeichert.................");

    toastr.clear();
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-full-width",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    };
    toastr.success("Erfolgreich gespeichert!<br /> Sie wurden aus Sicherheitsgründen jetzt ausgeloggt");

}



function resetData() {
    formFieldsArray.forEach(function (i) {
        i.value = "";
    });
}

var bearbeitenToggle = document.getElementById('bearbeitenToggler');
bearbeitenToggle.addEventListener('change', function () {
    if (bearbeitenToggle.checked == true) {
        enableMyDataFields();        
    } else {
        disableMyDataFields();
    }
});

function meinPasswortChange() {
    logout();
    showPageNonav('#iForgotPassword');
}

function meinPasswortChangeAreaOn() {
    $("#wirklichChangePassword").show();
    $("#myPasswordChangeButton").hide();
}

function meinPasswortChangeAreaOff() {
    $("#wirklichChangePassword").hide();
    $("#myPasswordChangeButton").show();
}

//--------------------------------------------- WaschPass Section--------------------------
var red = "#f7a5a5";
var green = "#82e251";
//doughnut

var gutsscheinAnzahl = 5;
var nochFehlendeWaschPass = 6 - gutsscheinAnzahl;
var nochFehlendeWaschPassAnker = document.getElementById('nochFehlendeWaschPass');
nochFehlendeWaschPassAnker.innerText = nochFehlendeWaschPass;

function donutMaker(gutscheinAnzahl) {
    // Chart.defaults.global.legend.display = false;
    var background = [];
    var donutPromise = new Promise(function (resolve, reject) {
        for (var k = 0; k < 6; k++) {
            if (k < gutscheinAnzahl) {
                background.push(green);
            } else {
                background.push(red);
            }
        }
        resolve(function () {
            return background;
        });
        // or
        reject("Error!");
    }).then(function () {
        var donutAnker = document.getElementById("doughnutChart").getContext('2d');
        var myChart = new Chart(donutAnker, {
            type: 'doughnut',
            data: {
                labels: [1, 2, 3, 4, 5, 6],
                datasets: [{

                    data: [166, 166, 166, 166, 166, 166],
                    backgroundColor: background,
                    borderColor: '#000',
                    borderWidth: 1,
                    position: 'bottom'

                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: false
                }
            }

        });

    });

}


donutMaker(gutsscheinAnzahl);


// -----------------------------------------------shopSection----------------------------------
var shopSumme = 0;
var lanzenwäscheSumme = 50;
var staubsaugerSumme = 8;
var wäscheSumme = 100;
var zahlungsMehtodeDIV = document.getElementById('zahlungsMethode');
var checkBoxWiederruf = document.getElementById('checkBoxWiederruf');
var zahlungsMehtodeBoolean = false;


$('#checkBoxLanzenwäsche').click(function () {
    if (this.checked) {
        shopSumme = shopSumme + lanzenwäscheSumme;
    } else {
        shopSumme = shopSumme - lanzenwäscheSumme;
        if (shopSumme <= 0) {
            zahlungsMethodeHIDE();
        }
    }
    var shopSummeInnerhtml = document.getElementById('shopSumme');
    shopSummeInnerhtml.textContent = " ";
    shopSummeInnerhtml.textContent = shopSumme;
    return shopSumme;
});
$('#checkBoxStaubsauger').click(function () {
    if (this.checked) {
        shopSumme = shopSumme + staubsaugerSumme;
    } else {
        shopSumme = shopSumme - staubsaugerSumme;
        if (shopSumme <= 0) {
            zahlungsMethodeHIDE();
        }
    }
    var shopSummeInnerhtml = document.getElementById('shopSumme');
    shopSummeInnerhtml.textContent = " ";
    shopSummeInnerhtml.textContent = shopSumme;
    return shopSumme;
});
$('#checkBoxWäsche').click(function () {
    if (this.checked) {
        shopSumme = shopSumme + wäscheSumme;
    } else {
        shopSumme = shopSumme - wäscheSumme;
        if (shopSumme <= 0) {
            zahlungsMethodeHIDE();
        }
    }
    var shopSummeInnerhtml = document.getElementById('shopSumme');
    shopSummeInnerhtml.textContent = " ";
    shopSummeInnerhtml.textContent = shopSumme;
    return shopSumme;
});

function disableShopCheckBoxes() { //<------------------ D I S able die Shop checkboxen
    var allCheckBoxes = document.getElementsByClassName('shopCheckBox');
    var bezahlenButtonTextDiv = document.getElementById('BezahlenButtonTextDiv');
    $('#bezahlenButton').addClass('bezahlenButtonDISable');
    bezahlenButtonTextDiv.textContent = "Warenkorb bearbeiten";
    for (p = 0; p < allCheckBoxes.length; p++) {
        allCheckBoxes[p].disabled = true;
    }
}

function enableShopCheckBoxes() {
    var allCheckBoxes = document.getElementsByClassName('shopCheckBox');
    var bezahlenButtonTextDiv = document.getElementById('BezahlenButtonTextDiv');
    var bezahlenButton = document.getElementById('bezahlenButton');
    $('#bezahlenButton').removeClass('bezahlenButtonDISable');
    bezahlenButtonTextDiv.textContent = "Weiter zur Zahlung";
    for (o = 0; o < allCheckBoxes.length; o++) {
        allCheckBoxes[o].disabled = false;
    }
}

function checkBoxReset() {
    var allCheckBoxes = document.getElementsByClassName('shopCheckBox');
    for (k = 0; k < allCheckBoxes.length; k++) {
        allCheckBoxes[k].checked = false;
    }
    shopSumme = 0;
    var shopSummeInnerhtml = document.getElementById('shopSumme');
    shopSummeInnerhtml.textContent = " ";
    shopSummeInnerhtml.textContent = shopSumme;
    return shopSumme;
}

$('#bezahlenButton').click(function () {
    if (shopSumme == 0) {
        toastr.info('Sie haben kein Produkt ausgewählt');
        return;
    }
    if (!checkBoxWiederruf.checked) {
        toastr.warning('Bitte das Widerrufsrecht durchlesen und anklicken');
        return;
    } else {
        if (!zahlungsMehtodeBoolean) {
            disableShopCheckBoxes();
            $('#zahlungsMethode').show();
            payUnit(shopSumme);
            zahlungsMehtodeBoolean = true;
            return zahlungsMehtodeBoolean;
        } else {
            zahlungsMethodeHIDE();
        }
    }
});


function zahlungsMethodeHIDE() {
    $('#zahlungsMethode').hide();
    var paymentWidgetsAnker = document.getElementById('paymentWidgetsAnker');
    if (paymentWidgetsAnker.firstChild != null) {
        while (paymentWidgetsAnker.firstChild) {
            paymentWidgetsAnker.removeChild(paymentWidgetsAnker.firstChild);
        }
    }
    zahlungsMehtodeBoolean = false;
    enableShopCheckBoxes();
    return zahlungsMehtodeBoolean;
}

function payUnit(summe) {
    var paystring =
        "authentication.userId=8a8294176059beb00160692d1852120e" +
        "&authentication.password=6tpwBHCk3e" +
        "&authentication.entityId=8a8294176059beb00160692d4b471213" +
        "&amount=" + summe +
        "&currency=EUR" +
        "&paymentType=DB" +
        "&registrations[0].id=8ac7a4a2698222870169901db1b12081" +
        "&registrations[2].id=8ac7a4a169912b2701699552a9532a98" +
        "&registrations[1].id=8ac7a49f698215e4016990b7e9606992";
    fetch('https://test.oppwa.com/v1/checkouts', {
        method: 'POST',
        body: paystring,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function (response) {
        return response.json();
    }).then(function (data) {
        console.log(data);
        console.log(data.id);
        console.log(data.result.code);
        if (data.id != null || data.id == "") {
            var paymentWidgetsAnker = document.getElementById('paymentWidgetsAnker');

            var script = document.createElement('script');
            $(script).attr({
                "src": "https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=" + data.id
            });
            if (Hosting === "http://localhost") {
                var localPort = "http://localhost:8080";            // Achtung das muss bei RELEASE raus 
                Hosting = localPort;
                
            }
            paymentWidgetsAnker.appendChild(script);
            var payUnityForm = document.createElement('form');
            $(payUnityForm).attr({
                "action": Hosting + "/response2user/payResponse.html",
                "class": "paymentWidgets",
                "data-brands": "VISA MASTER SOFORTUEBERWEISUNG",
                "lang": "de",
                // "name": "myCustomIframe",
                // "target": "myCustomIframe",
            });
            paymentWidgetsAnker.appendChild(payUnityForm);
            

        }
        Hosting = "http://localhost";
    }).catch(function (err) {
        console.log("Hier ist ein PayUnity fehler: " + err);
    });

}


// ---------------------Logout-----------------------------------------

function logout() {
    meinPasswortChangeAreaOff();
    zahlungsMethodeHIDE();
    zahlungsMehtodeBoolean = false;
    checkBoxReset();
    location.hash = "";
    resetData();
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-full-width",
        "preventDuplicates": true,
        "showDuration": "200",
        "hideDuration": "500",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr.info('Erfolgreich ausgeloggt');
    if (sessionStorage != null || localStorage != null) {
        var logoutsessionValue = sessionStorage.getItem('hash');
        fetch(Hosting + ':8500/app/logout', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'

            },
            body: JSON.stringify(logoutsessionValue)
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            if (data.successful == true) {
                sessionStorage.removeItem('email', 'value');
                sessionStorage.removeItem('hash', 'value');
                localStorage.removeItem('email');
                localStorage.removeItem('hash');
                //Hier entleeren wir die Stationen für jeden Node ab Anker
                var stationAnkerEntleeren = document.getElementById('nav-tabContent');
                var stationAnker2Entleeren = document.getElementById('list-tab');
                if (stationAnkerEntleeren.firstChild != null) {
                    while (stationAnkerEntleeren.firstChild) {
                        stationAnkerEntleeren.removeChild(stationAnkerEntleeren.firstChild);
                        stationAnker2Entleeren.removeChild(stationAnker2Entleeren.firstChild);
                    }
                }

                var emptyQrAnker = document.getElementById('qrAnker');
                emptyQrAnker.removeChild(emptyQrAnker.firstChild);
            }
            // showPageNonav('#loginSection');
        }).catch(function (err) {
            sessionStorage.removeItem('email', 'value');
            sessionStorage.removeItem('hash', 'value');
            localStorage.removeItem('email');
            localStorage.removeItem('hash');
            if (stationAnkerEntleeren.firstChild != null) {
                while (stationAnkerEntleeren.firstChild) {
                    stationAnkerEntleeren.removeChild(stationAnkerEntleeren.firstChild);
                    stationAnker2Entleeren.removeChild(stationAnker2Entleeren.firstChild);
                }
            }
            var emptyQrAnker = document.getElementById('qrAnker');
            emptyQrAnker.removeChild(emptyQrAnker.firstChild);
            console.log("Hier ist ein fehler: " + err);
        });
        return showPageNonav('#loginSection');
    }

}

$('.list-group').on('click', function (e) {
    var page = document.getElementById('stationenSection');
    $('html, body').animate({
        scrollTop: $(page).offset().top - 60
    }, 'slow');
    // e.preventDefault();
});

loadingSpinnerEIN();

// Gutschein Zentrierung der CollapseKarten
$(document).on("shown.bs.collapse", ".collapse", function (e) {
    var $card = $(this).closest('.card');
    $('html,body').animate({
        scrollTop: $card.offset()
    }, 100);
});

// show.bs.collaps -> bevor aufgeklappt wird
// shown(!N).bs.collaps -> nachdem aufgeklappt wurde