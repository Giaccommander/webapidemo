self.addEventListener('install', function (event) {
    event.waitUntil(caches.open('static')
        .then(function (cache) {
            cache.addAll([
                '/',
                '/index.html',
                '/agb.html',
                '/offline.html',
                'src/css/font-awesome.css',                
                'src/css/bootstrap.css',                               
                'src/css/mdb.css',
                'src/css/toastr.css',                
                '/src/css/homeSPA.css',
                'src/scripts/jquery.js',
                'src/scripts/jquery-3.3.1.min.js',
                'src/scripts/jquery.qrcode.min.js',
                'src/scripts/qrcode.js',
                'src/scripts/popper.min.js',
                'src/scripts/bootstrap.js',                                
                'src/scripts/toastr.js',                
                '/src/js/app.js',
                'src/js/gutscheine.js',
                'src/js/stationen.js',                
                'src/js/idb.js',
                'src/images/icons',
                'src/images/AGB_Leikermoser_Januar_2018.pdf',
                'src/font/roboto/Roboto-Light.ttf',
                'src/font/roboto/Roboto-Bold.ttf',
                'src/font/roboto/Roboto-Regular.ttf',
                'src/images/InkedMetaslider_Tankstelle1_1920x7004-0x600_LI.jpg',
                'src/images/icons/app-icon-144x144.png',
                'src/images/LM_Barrabatt.gif',
                '/src/images/tankstellen.png',
                'src/images/nur_OTAS.png',
                'src/images/offlineFloat.png',
                '/favicon.ico'


            ]);
        })
    );
});

self.addEventListener('activate', function (event) {
    return self.clients.claim();
});

var SW_Version = "Powered by OTAS v1.2.3";

// --------------------------------------------- Database------------------------------------------------
(function () {
    'use strict';

    //check for support
    if (!self.indexedDB) {
        console.log('This browser doesn\'t support IndexedDB');
        return;
    } else {

        if (typeof idb === "undefined") {
            self.importScripts('src/js/idb.js');
            console.log("idb-Ready@ ServiceWorker");
        }
    }
})();


var getVoucherURL = 'http://localhost:8500/app/voucherlist';



var db;

function dbDown(db) {

    db.onversionchange = function (event) {
        db.close();
        console.log("A new version of this page is ready. Please reload!");
    };
}

function DBPromise() {
    var dbName = "otasAppDB";

    var request = indexedDB.open(dbName, 1);
    
    request.onerror = function (event) {
        console.log("DB " + dbName + " ,konnte nicht geöffnet werden. ERROR: " + event);
    };

    request.onblocked = function(event) {
        console.log("onblocked ERROR: " + event);
        
    };
    request.onupgradeneeded = function (event) {
        db = event.target.result;
        var objectStore = db.createObjectStore("Voucher", {
            keyPath: "ConsecutiveVoucherNo"
        });
        objectStore.createIndex("VoucherId", "VoucherId", {
            unique: true
        });
        objectStore.transaction.oncomplete = function (event) {
           
            var customerObjectStore = db.transaction("Voucher", "readwrite").objectStore("Voucher");

            VoucherTempArray.forEach(function (VoucherCollection) {

                customerObjectStore.add(VoucherCollection);
            });
            dbDown(db);
        };    
    };
}

var VoucherTempArray = [];
self.addEventListener('fetch', function (event) {
    if (event.request.url.indexOf(getVoucherURL) > -1) {
        fetch(event.request)
            .then(function (response) {
                return response.json();
            }).then(function (data) {
                var gutscheine = data;
                for (var i = 0; i < gutscheine.VoucherCollection.length; i++) {
                    VoucherTempArray.push(gutscheine.VoucherCollection[i]);
                }


            }).then(function () {            

                DBPromise();
               
            }).catch(function (err) {
                console.log(err);
                return caches.open("static")
                    .then(function (cache) {
                        return cache.match('/offline.html');
                    });

            });
    } else {
        event.respondWith(
            caches.match(event.request)
            .then(function (response) {
                if (response) {
                    return response;
                } else {
                    return fetch(event.request);
                }
            }).catch(function (err) {
                return caches.open('static')
                    .then(function (cache) {
                        return cache.match('/offline.html');
                    });
            })
        );
    }
});

var IOS=false;
if ((navigator.platform.indexOf("iPhone") != -1) ||
    (navigator.platform.indexOf("iPod") != -1) ||
    (navigator.platform.indexOf("iPad") != -1)) {
     IOS=true;
} else {
    var channel = new BroadcastChannel('sw-messages');
    channel.postMessage({
        title: SW_Version
    });
}
