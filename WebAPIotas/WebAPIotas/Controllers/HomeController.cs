﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WebAPIotas.Models;
using HttpPostAttribute = System.Web.Mvc.HttpPostAttribute;

namespace WebAPIotas.Controllers
{
    public class HomeController : Controller
    {
        [RequireHttps]
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [HttpPost]
        [RequireHttps]
        public ActionResult Login(LoginModel fromdata)
        {
            User person = new User();
            DataAccess db = new DataAccess();
            person = db.GetUserDb(fromdata.Email);
            ResponseObject response = new ResponseObject();
            if (person != null)
            {
                response.code = 200;
                response.description = "Login erfolgreich";
                response.status = "success";
                response.value = " ";
                response.Hash = person.Activationhash;
                string personActiAt = person.ActivatedAt.ToString();
                string personRegAt = person.RegisteredAt.ToString();
                string personId = person.Id.ToString();
                response.value += person.Anrede += person.FirstName += person.LastName += person.Street += person.Zip += person.Country += person.Email += person.Land += person.TelefonNumber += personRegAt += personActiAt += personId;           
            }
            else
            {
                response.code = 400;
                response.description = "Diese Emailadresse wurde nicht gefunden";
                response.status = "not found";
                response.value = "Bitte Registrieren Sie sich zuerst";
            }
            return Json(response);
        }


        [HttpPost]
        [RequireHttps]
        public ActionResult Register(RegisterModel regData)
        {
            RegisterResponseObject response = new RegisterResponseObject();
            foreach (var e in regData)
            {
                if (e.ToString() == " " || e == null)
                {
                    response.code = 200;
                    response.description = "Eines der Felder ist nicht ordnungsgemäß ausgefüllt.";
                    response.status = "error";
                    response.value = "JavaScript Fehler.";
                    response.httpResponse = "https://localhost:44325/api";

                    return Json(response);
                }
            }

            return Json(response);
        }

    }
}
