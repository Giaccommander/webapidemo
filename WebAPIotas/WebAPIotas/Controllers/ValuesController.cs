﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace WebAPIotas.Controllers
{
    
    public class ValuesController : ApiController
    {

        // GET api/values
        [RequireHttps]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [RequireHttps]
        public string Get(int id)
        {
            return "value "+ id;
        }

        // POST api/values
        [RequireHttps]
        public void Post([FromBody]string value)
        {

            
            
        }


    }
}
